package com.example.getfitness.model.relations;

import androidx.annotation.NonNull;
import androidx.room.Entity;

// Many to Many relation

@Entity(primaryKeys = {"eventId", "userId"})
public class EventUser {
    @NonNull
    public String eventId;
    @NonNull
    public String userId;

    public Boolean deleted;

    public EventUser(@NonNull String eventId, @NonNull String userId, Boolean deleted) {
        this.eventId = eventId;
        this.userId = userId;
        this.deleted = deleted;
    }

    public EventUser() {

    }

    @NonNull
    public String getEventId() {
        return eventId;
    }

    public void setEventId(@NonNull String eventId) {
        this.eventId = eventId;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NonNull String userId) {
        this.userId = userId;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
