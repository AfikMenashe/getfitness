package com.example.getfitness.model;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.getfitness.model.DAOs.EventDao;
import com.example.getfitness.model.DAOs.EventTypeDao;
import com.example.getfitness.model.DAOs.EventUserDao;
import com.example.getfitness.model.DAOs.ImageUrlDao;
import com.example.getfitness.model.DAOs.ReviewDao;
import com.example.getfitness.model.DAOs.TrainerDao;
import com.example.getfitness.model.DAOs.TypeDao;
import com.example.getfitness.model.DAOs.UserDao;
import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.ImageUrl;
import com.example.getfitness.model.entites.Review;
import com.example.getfitness.model.entites.Trainer;
import com.example.getfitness.model.entites.Type;
import com.example.getfitness.model.entites.User;
import com.example.getfitness.model.relations.EventType;
import com.example.getfitness.model.relations.EventUser;

@Database(entities = {User.class, Trainer.class, Event.class, Review.class, Type.class, EventUser.class, EventType.class, ImageUrl.class}, version = 18)
@TypeConverters({Converters.class})
abstract class AppLocalDbRepository extends RoomDatabase {
    public abstract UserDao userDao();

    public abstract TrainerDao trainerDao();

    public abstract ReviewDao reviewDao();

    public abstract TypeDao typeDao();

    public abstract EventDao eventDao();

    public abstract EventUserDao eventUserDao();

    public abstract EventTypeDao eventTypeDao();

    public abstract ImageUrlDao imageUrlDao();

}

public class AppLocalDb {
    static public final AppLocalDbRepository db =
            Room.databaseBuilder(MyApplication.context,
                    AppLocalDbRepository.class,
                    "dbFileName.db")
                    .fallbackToDestructiveMigration()
                    .build();
}