package com.example.getfitness.model.DAOs;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.Review;
import com.example.getfitness.model.entites.Trainer;
import com.example.getfitness.model.entites.User;
import com.example.getfitness.model.relations.UserWithEvents;

import java.util.List;

@Dao
public interface TrainerDao {
    @Query("SELECT * FROM Trainer")
    LiveData<List<Trainer>> getAll();

    @Query("SELECT * FROM Trainer WHERE Trainer.trainerId=:trainerId")
    LiveData<Trainer> getTrainer(String trainerId);

    @Query("SELECT * FROM Event WHERE Event.trainerCreatorId=:trainerId")
    LiveData<List<Event>> getTrainerEvents(String trainerId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Trainer trainers);

    @Delete
    void delete(Trainer trainer);
}
