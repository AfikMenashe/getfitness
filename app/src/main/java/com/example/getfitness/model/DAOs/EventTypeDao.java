package com.example.getfitness.model.DAOs;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.sqlite.db.SupportSQLiteQuery;

import com.example.getfitness.model.relations.EventType;
import com.example.getfitness.model.relations.EventWithTypes;
import com.example.getfitness.model.relations.TypeWithEvents;

import java.util.List;

@Dao
public interface EventTypeDao {


    @Query("SELECT * FROM EventType WHERE EventType.eventId=:eventId AND EventType.typeId=:typeId")
    LiveData<EventType> getEventType(String eventId, String typeId);

    @Query("SELECT * FROM Type")
    LiveData<List<TypeWithEvents>> getTypeWithEventsAll();

//    @Query("SELECT * FROM Type WHERE Type.typeId =:typeId or Type.typeId =:second")
//    LiveData<TypeWithEvents> getTypeWithEvents(String typeId, String second);

    @RawQuery
    LiveData<TypeWithEvents> getTypeWithEvents(SupportSQLiteQuery query);

    @Query("SELECT * FROM Event")
    LiveData<List<EventWithTypes>> getEventWithTypesAll();

    @Query("SELECT * FROM Event WHERE Event.eventId =:eventId")
    LiveData<EventWithTypes> getEventWithTypes(String eventId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(EventType... eventTypes);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllEventTypes(List<EventType> eventTypes);

    @Delete
    void delete(EventType eventType);

}