package com.example.getfitness.model.DAOs;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.getfitness.model.relations.EventUser;
import com.example.getfitness.model.relations.EventWithUsers;
import com.example.getfitness.model.relations.UserWithEvents;

import java.util.List;

@Dao
public interface EventUserDao {
    @Query("SELECT * FROM Event")
    LiveData<List<EventWithUsers>> getEventWithUsersAll();

    @Query("SELECT * FROM EventUser WHERE EventUser.eventId=:eventId AND EventUser.userId=:userId")
    LiveData<EventUser> getEventUser(String eventId, String userId);

    @Query("SELECT * FROM User")
    LiveData<List<UserWithEvents>> getUserWithEventsAll();

    @Query("SELECT * FROM User WHERE User.userId=:userId")
    LiveData<UserWithEvents> getUserWithEvents(String userId);

    @Query("SELECT * FROM Event WHERE Event.eventId=:eventId")
    LiveData<EventWithUsers> getEventWithUsers(String eventId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(EventUser... eventUser);

    @Delete
    void delete(EventUser eventUser);

}
