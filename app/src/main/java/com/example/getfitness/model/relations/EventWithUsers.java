package com.example.getfitness.model.relations;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.User;

import java.util.List;

public class EventWithUsers {
    @Embedded
    public Event event;
    @Relation(
            parentColumn = "eventId",
            entityColumn = "userId",
            associateBy = @Junction(EventUser.class)
    )
    public List<User> users;
}
