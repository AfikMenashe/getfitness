package com.example.getfitness.model.entites;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;
import java.util.UUID;

@Entity
public class Type {
    @ColumnInfo
    @PrimaryKey
    @NonNull
    String typeId;

    String name;

    long lastUpdated;

    public Boolean deleted;

    public Type(String name, Boolean deleted) {
        this.typeId = String.valueOf(UUID.randomUUID());

        this.name = name;
        this.lastUpdated = new Date().getTime();
        this.deleted = deleted;
    }

    public Type() {
        this.typeId = String.valueOf(UUID.randomUUID());
        this.lastUpdated = new Date().getTime();
    }
//----------------------------Getters and Setters----------------------------//


    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
