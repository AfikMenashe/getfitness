package com.example.getfitness.model.entites;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;
import java.util.UUID;

@Entity
public class Trainer {
    @ColumnInfo
    @PrimaryKey
    @NonNull
    String trainerId;

    String email;

    String name;

    String birthday;

    String phone;
    String imageUrl;

    long lastUpdated;
    boolean isDeleted;
    //----------------------------Constructors----------------------------//

    public Trainer(String trainerId, String email, String name, String birthday, String phone,String imageUrl) {
        this.trainerId = trainerId;
        this.email = email;
        this.name = name;
        this.birthday = birthday;
        this.phone = phone;
        this.lastUpdated = new Date().getTime();
        this.isDeleted = false;
        if (imageUrl.equals(""))
            this.imageUrl = "https://www.idea-alm.com/wp-content/uploads/2019/01/blank-profile-picture-973460_960_720.png";
        else
            this.imageUrl = imageUrl;
    }

    public Trainer() {

    }
//----------------------------Getters and Setters----------------------------//


    public String getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(String trainerId) {
        this.trainerId = trainerId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setAll(String trainerId, String email, String name, String birthday, String phone, String imageUrl) {
        this.trainerId = trainerId;
        this.email = email;
        this.name = name;
        this.birthday = birthday;
        this.phone = phone;
        this.lastUpdated = new Date().getTime();
        this.isDeleted = false;
        this.imageUrl = imageUrl;
    }
}
