package com.example.getfitness.model.relations;

import androidx.annotation.NonNull;
import androidx.room.Entity;

import java.util.Date;

// Many to Many relation

@Entity(primaryKeys = {"eventId", "typeId"})
public class EventType {
    @NonNull
    public String eventId;
    @NonNull
    public String typeId;

    public Boolean deleted;

    public long lastUpdated;

    public EventType(@NonNull String eventId, @NonNull String typeId, Boolean deleted) {
        this.eventId = eventId;
        this.typeId = typeId;
        this.deleted = deleted;
        this.lastUpdated = new Date().getTime();
    }

    public EventType() {

    }

    @NonNull
    public String getEventId() {
        return eventId;
    }

    public void setEventId(@NonNull String eventId) {
        this.eventId = eventId;
    }

    @NonNull
    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(@NonNull String typeId) {
        this.typeId = typeId;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
