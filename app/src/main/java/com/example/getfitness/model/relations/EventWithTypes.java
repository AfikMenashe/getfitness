package com.example.getfitness.model.relations;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.Type;

import java.util.List;

//returns Event with all the Types

public class EventWithTypes {
    @Embedded
    public Event event;
    @Relation(
            parentColumn = "eventId",
            entityColumn = "typeId",
            associateBy = @Junction(EventType.class)
    )
    public List<Type> types;
}