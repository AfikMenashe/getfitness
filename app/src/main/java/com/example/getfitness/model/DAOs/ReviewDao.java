package com.example.getfitness.model.DAOs;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.Review;

import java.util.List;

@Dao
public interface ReviewDao {
    @Query("SELECT * FROM Review")
    LiveData<List<Review>> getAll();

    @Query("SELECT * FROM Review WHERE Review.reviewId=:reviewId")
    LiveData<Review> getReview(String reviewId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Review... reviews);

    @Delete
    void delete(Review review);

}