package com.example.getfitness.model.entites;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;
import java.util.UUID;

@Entity
public class Review {
    @ColumnInfo
    @PrimaryKey
    @NonNull
    String reviewId;

    long authorId;

    String content;

    Double rating;

    Date date;

    String trainerAboutId;

    long lastUpdated;
    //----------------------------Constructors----------------------------//

    public Review(long authorId, String content, Double rating, Date date, String trainerAboutId) {
        this.reviewId = String.valueOf(UUID.randomUUID());
        this.authorId = authorId;
        this.content = content;
        this.rating = rating;
        this.date = date;
        this.trainerAboutId = trainerAboutId;
        this.lastUpdated = new Date().getTime();
    }

    public Review() {
        this.reviewId = String.valueOf(UUID.randomUUID());
        this.lastUpdated = new Date().getTime();
    }
    //----------------------------Getters and Setters----------------------------//


    public String getReviewId() {
        return reviewId;
    }

    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    public long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTrainerAboutId() {
        return trainerAboutId;
    }

    public void setTrainerAboutId(String trainerAboutId) {
        this.trainerAboutId = trainerAboutId;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
