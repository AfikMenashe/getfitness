package com.example.getfitness.model;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.LiveData;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.getfitness.controllers.MainActivity;
import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.ImageUrl;
import com.example.getfitness.model.entites.Review;
import com.example.getfitness.model.entites.Trainer;
import com.example.getfitness.model.entites.Type;
import com.example.getfitness.model.entites.User;
import com.example.getfitness.model.relations.EventType;
import com.example.getfitness.model.relations.EventUser;
import com.example.getfitness.model.relations.EventWithImageUrls;
import com.example.getfitness.model.relations.EventWithTypes;
import com.example.getfitness.model.relations.EventWithUsers;
import com.example.getfitness.model.relations.TypeWithEvents;
import com.example.getfitness.model.relations.UserWithEvents;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

public class Model {
    public final static Model instance = new Model();
    private final ModelSQL modelSql = new ModelSQL();
    private MainActivity mainActivity;

    private final ModelFirebase modelFirebase = new ModelFirebase();
    private LiveData<List<User>> usersList;
    private LiveData<List<Trainer>> trainersList;
    private LiveData<List<Event>> eventsList;
    private LiveData<List<Review>> reviewsList;
    private LiveData<List<Type>> typesList;
    final SharedPreferences sp;


    private Model() {
        sp = MyApplication.context.getSharedPreferences("Event", Context.MODE_PRIVATE);

    }

    interface ModelListener<T> { // generic interface for listeners
        void onComplete();
    }

    public SharedPreferences getSp() {
        return sp;
    }

    // <----------------------------- User ----------------------------->
    public void insertUser(User user, ModelSQL.AddListener listener) {
        modelSql.addUser(user, listener);
        modelFirebase.addUser(user);
    }

    public void deleteUser(User user, ModelSQL.AddListener listener) {
        modelSql.deleteUser(user, listener);
        modelFirebase.deleteUser(user);
    }

    public void getUserFirebaseToROOM(String userId) {
        modelFirebase.getUser(userId, task -> {
            if (!task.isSuccessful()) {
                Log.e("Firebase", "Error getting data", task.getException());
            } else {
                //insert to local DB the user
                modelSql.addUser(task.getResult().getValue(User.class), null);
            }
        });
    }

    public LiveData<List<User>> getUsersList() {
        if (usersList == null)
            usersList = modelSql.getUsersList();
        return usersList;
    }

    public LiveData<User> getUser(String userId) {
        return modelSql.getUser(userId);
    }

    // <----------------------------- EventUser ----------------------------->
    public void addUserToEvent(EventUser eventUser, ModelSQL.AddListener listener) {
        modelSql.addUserToEvent(eventUser, listener);
        modelFirebase.addUserToEvent(eventUser);
    }

    public LiveData<EventUser> getEventUser(String eventId, String userId) {
        return modelSql.getEventUser(eventId, userId);
    }

    //return User with all his events
    public LiveData<UserWithEvents> getUserWithEvents(String userId) {
        return modelSql.getUserWithEvents(userId);
    }

    //return Event with all his users
    public LiveData<EventWithUsers> getEventWithUsers(String eventId) {
        return modelSql.getEventWithUsers(eventId);
    }

    public void deleteUserFromEvent(EventUser eventUser) {
        modelSql.deleteUserFromEvent(eventUser);
        modelFirebase.deleteUserFromEvent(eventUser);
    }

    // <----------------------------- Trainer ----------------------------->

    public void insertTrainer(Trainer trainer, ModelSQL.AddListener listener) {
        modelSql.addTrainer(trainer, listener);
        modelFirebase.addTrainer(trainer);
    }

    public void deleteTrainer(Trainer trainer, ModelSQL.AddListener listener) {
        modelSql.deleteTrainer(trainer, listener);
    }

    public LiveData<List<Trainer>> getTrainersList() {
        if (trainersList == null)
            trainersList = modelSql.getTrainersList();
        return trainersList;
    }

    public LiveData<Trainer> getTrainer(String trainerId) {
        return modelSql.getTrainer(trainerId);
    }

    public void getTrainerFirebaseToROOM(String trainerId) {
        modelFirebase.getTrainer(trainerId, new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                if (!task.isSuccessful()) {
                    Log.e("Firebase", "Error getting data", task.getException());
                } else {
                    //insert to local DB the user
                    modelSql.addTrainer(task.getResult().getValue(Trainer.class), null);
                }
            }
        });
    }
    // <----------------------------- Review ----------------------------->

    public void insertReview(Review review, ModelSQL.AddListener listener) {
        modelSql.addReview(review, listener);
    }

    public LiveData<List<Review>> getReviewList() {
        if (reviewsList == null)
            reviewsList = modelSql.getReviewsList();
        return reviewsList;
    }

    public LiveData<Review> getReview(String reviewId) {
        return modelSql.getReview(reviewId);
    }

    // <----------------------------- Type ----------------------------->

    public void insertType(Type type, ModelSQL.AddListener listener) {
        modelSql.addType(type, listener);
        modelFirebase.addType(type);
    }

    public LiveData<List<Type>> getTypeList() {
        if (typesList == null)
            typesList = modelSql.getTypeList();
        return typesList;
    }

    public LiveData<Type> getType(String typeId) {
        return modelSql.getType(typeId);
    }

    public void deleteType(Type type) {
        modelSql.deleteType(type);
    }

    //---------------------------- ImageUrl Entity ----------------------------//
    public void insertImageUrl(ImageUrl imageUrl, ModelSQL.AddListener listener) {
        modelSql.addImageUrl(imageUrl, listener);
        modelFirebase.addImageUrl(imageUrl);
    }

    public LiveData<ImageUrl> getImageUrl(String imageUrlId) {
        return modelSql.getImageUrl(imageUrlId);
    }

    //return Event with all his imageUrls
    public LiveData<List<EventWithImageUrls>> getEventWithImageUrls(String eventId) {
        return modelSql.getEventWithImageUrls(eventId);
    }

    public void deleteImageUrl(ImageUrl imageUrl) {
        modelSql.deleteImageUrl(imageUrl,null);
        modelFirebase.deleteImageUrl(imageUrl);
    }

    public LiveData<List<ImageUrl>>  getAllImageUrl(){
        return modelSql.getAllImageUrl();
    }


    // <----------------------------- EventType ----------------------------->
    public void addTypeToEvent(EventType eventType, ModelSQL.AddListener listener) {
        modelSql.addTypeToEvent(eventType, listener);
        modelFirebase.addTypeToEvent(eventType);
    }

    public void addManyTypeToEvent(List<EventType> manyEventType, ModelSQL.AddListener listener) {
        modelSql.addManyTypeToEvent(manyEventType, listener);
        modelFirebase.addManyTypeToEvent(manyEventType);
    }

    public LiveData<EventType> getEventType(String eventId, String typeId) {
        return modelSql.getEventType(eventId, typeId);
    }

    //return Type with all his events
    public LiveData<TypeWithEvents> getTypeWithEvents(ArrayList<String> typyNames) {
        return modelSql.getTypeWithEvents(typyNames);
    }

    //return Event with all his users
    public LiveData<EventWithTypes> getEventWithTypes(String eventId) {
        return modelSql.getEventWithTypes(eventId);
    }

    public void deleteTypeFromEvent(EventType eventType) {
        modelSql.deleteTypeFromEvent(eventType);
        modelFirebase.deleteTypeFromEvent(eventType);
    }


    // <----------------------------- Event ----------------------------->

    public void insertEvent(Event event, ModelSQL.AddListener listener) {
        modelSql.addEvent(event, listener);
        modelFirebase.addEvent(event);
    }

    public LiveData<List<Event>> getEventsList() {
        if (eventsList == null) {
            eventsList = modelSql.getEventsList();
//            refreshAllEvents();
        }
        return eventsList;
    }

    public LiveData<Event> getEvent(String eventId) {
        return modelSql.getEvent(eventId);
    }

    public void deleteEvent(Event event, ModelSQL.AddListener listener) {
        modelSql.deleteEvent(event, listener);
        modelFirebase.deleteEvent(event);
    }

    public void refreshDB(String userId, SwipeRefreshLayout mySwipeRefreshLayout) {
        // get from SharedPreferences the last update time
        final Long lastUpdated = sp.getLong("lastUpdated", 0);
//        final Long lastUpdated = Long.valueOf(0); // TODO: Remmber to change this to work like regular app
        // get from Firebase Events that their last update is greater than the app

//        // get the ImageUrls from Firebase
        modelFirebase.refreshImageUrlsDB(lastUpdated, new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                long lastU = lastUpdated;
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    ImageUrl imageUrl = postSnapshot.getValue(ImageUrl.class);
                    if (imageUrl.getLastUpdated() > lastU) { // if imageUrl update is greater than the app last update
                        if (!imageUrl.getDeleted()) // verify the imageUrl is not deleted
                            modelSql.addImageUrl(imageUrl, null);
                        else
                            modelSql.deleteImageUrl(imageUrl, null);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("firebase", "Get ImageUrls from firebase Cancelled");

            }
        });

        // get the EventUsers from Firebase > only the event the user sign up to
        modelFirebase.refreshEventUsersDB(userId, new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    EventUser eventUser = postSnapshot.getValue(EventUser.class);
                    if (!eventUser.getDeleted()) // verify the eventUser is not deleted
                        modelSql.addUserToEvent(eventUser, null);
                    else
                        modelSql.deleteUserFromEvent(eventUser);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        // get the Trainers from Firebase
        modelFirebase.refreshTrainersDB(lastUpdated, new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                long lastU = lastUpdated;
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    Trainer trainer = postSnapshot.getValue(Trainer.class);
                    if (trainer.getLastUpdated() > lastU) { // if trainer update is greater than the app last update
                        if (!trainer.isDeleted()) // verify the trainer is not deleted
                            modelSql.addTrainer(trainer, null);
                        else
                            modelSql.deleteTrainer(trainer, null);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("Firebase", "Get Types from firebase Cancelled");

            }
        });
        // get the Types from Firebase
        modelFirebase.refreshTypesDB(lastUpdated, new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                long lastU = lastUpdated;
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    Type type = postSnapshot.getValue(Type.class);
                    if (type.getLastUpdated() > lastU) { // if type update is greater than the app last update
                        if (!type.getDeleted()) // verify the type is not deleted
                            modelSql.addType(type, null);
                        else
                            modelSql.deleteType(type);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("Firebase", "Get Types from firebase Cancelled");

            }
        });
        // get the EventTypes from Firebase
        modelFirebase.refreshEventTypesDB(lastUpdated, new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                long lastU = lastUpdated;
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    EventType eventType = postSnapshot.getValue(EventType.class);
                    if (eventType.getLastUpdated() > lastU) { // if eventType update is greater than the app last update
                        if (!eventType.getDeleted()) // verify the eventType is not deleted
                            modelSql.addTypeToEvent(eventType, null);
                        else
                            modelSql.deleteTypeFromEvent(eventType);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("Firebase", "Get EventTypes from firebase Cancelled");

            }
        });
        // get the Events from Firebase
        modelFirebase.refreshEventsDB(lastUpdated, new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                long lastU = lastUpdated;
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    Event event = postSnapshot.getValue(Event.class);
                    if (event.getLastUpdated() > lastU) { // if event update is greater than the app last update
                        if (!event.isDeleted()) // verify the event is not deleted
                            modelSql.addEvent(event, null);
                        else
                            modelSql.deleteEvent(event, null);
                        // update the last update
                        lastU = event.getLastUpdated();
                    }
                }
                sp.edit().putLong("lastUpdated", lastU).apply();
                mySwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("Firebase", "Get Events from firebase Cancelled");
            }
        });
    }

    public LiveData<List<Event>> getActiveTrainerEvents(String trainerID) {
        return modelSql.getTrainerEvents(trainerID);
    }

    public LiveData<List<Event>> getEventsByDates(Date from, Date to) {
        return modelSql.getEventsByDates(from, to);
    }

    public void addCountSignUserToEvent(String eventId, int count) {
        modelFirebase.addCountSignUserToEvent(eventId, count);
    }

    public void removeCountSignUserToEvent(String eventId, int count) {
        modelFirebase.removeCountSignUserToEvent(eventId, count);
    }
}
