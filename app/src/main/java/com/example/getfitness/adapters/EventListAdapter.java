package com.example.getfitness.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.example.getfitness.R;
import com.example.getfitness.model.Model;
import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.relations.EventWithImageUrls;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.ViewHolder> implements Filterable {

    private final LifecycleOwner lifecycleOwner;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    // <------------- Data Members ------------->

    public List<Event> eventsList;
    public List<Event> eventsList2 = new ArrayList<>();
    private OnItemClickListener listener;

    // <------------- Constructor and functions ------------->

    public EventListAdapter(LifecycleOwner lifecycleOwner) {

        this.lifecycleOwner = lifecycleOwner;
    }

    public EventListAdapter(List<Event> eventsList, LifecycleOwner lifecycleOwner) {
        this.eventsList = eventsList;
        this.eventsList2 = eventsList;
        this.lifecycleOwner = lifecycleOwner;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }


    Filter filter = new Filter() {

        //run on background
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            List<Event> FilteredList = new ArrayList<>();
            if (constraint.toString().isEmpty()) {
                FilteredList.addAll(eventsList);
            } else {
                for (Event search : eventsList) {
                    if (search.getTitle().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        FilteredList.add(search);
                    }

                }
            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = FilteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            eventsList2 = new ArrayList<>();
            eventsList2.addAll((Collection<? extends Event>) results.values);
            notifyDataSetChanged();
        }
    };


    public void setOnClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    // <------------- ViewHolder ------------->


    @NonNull
    @Override
    public EventListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_event, parent, false);
        return new ViewHolder(view, listener);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull EventListAdapter.ViewHolder holder, int position) {
        Event event = eventsList2.get(position);
        holder.position = position;
        holder.eventTitle.setText(event.getTitle());
        holder.eventCity.setText(event.getCity());
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf  = new SimpleDateFormat("yyyy.MMMM.dd hh:mm aaa");
        holder.eventDate.setText(sdf.format(event.getDate()));
        holder.eventPrice.setText(event.getPrice() + "₪");
        Model.instance.getEventWithImageUrls(event.getEventId()).observe(lifecycleOwner, new Observer<List<EventWithImageUrls>>() {
            @Override
            public void onChanged(List<EventWithImageUrls> eventWithImageUrls) {
                if (!eventWithImageUrls.isEmpty())
                    if (eventWithImageUrls.get(0).imageUrls.size() > 0)
                        if (eventWithImageUrls.get(0).imageUrls.get(0).getUrl() != null)
                            Picasso.get().load(eventWithImageUrls.get(0).imageUrls.get(0).getUrl()).fit().into(holder.eventImage);
            }
        });
    }

    public void setEvents(List<Event> events) {
        this.eventsList = events;
        this.eventsList2 = events;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (this.eventsList2 != null)
            return this.eventsList2.size();
        else return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //The view holder job is to save the reference to the UI objects of row
        int position;
        TextView eventTitle;
        TextView eventDate;
        TextView eventCity;
        TextView eventPrice;
        ImageView eventImage;


        public ViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            eventTitle = itemView.findViewById(R.id.titleEvent);
            eventCity = itemView.findViewById(R.id.event_location);
            eventDate = itemView.findViewById(R.id.event_date);
            eventPrice = itemView.findViewById(R.id.event_price);
            eventImage = itemView.findViewById(R.id.event_image);
            itemView.setOnClickListener(v -> listener.onItemClick(position));
        }

    }

}
