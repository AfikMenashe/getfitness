package com.example.getfitness.controllers.loginFlow;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.getfitness.R;
import com.example.getfitness.controllers.MainActivity;
import com.example.getfitness.model.Model;
import com.example.getfitness.model.ModelSQL;
import com.example.getfitness.model.entites.Trainer;
import com.example.getfitness.model.entites.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;

public class SignUpFragment extends Fragment {
    //   <--------------- Screen --------------->
    EditText _fullNameText;
    EditText _emailText;
    EditText _passwordText;
    Button _signupButton;
    RadioGroup _userType;
    FirebaseUser user;
    View view;

    EditText birthday;
    EditText phone;
    Button saveBtn;
    Button btnSelect;
    ImageView imageView;
    FirebaseStorage storage;
    StorageReference storageReference;
    String imageURL;

    //   <--------------- FirebaseAuth --------------->
    private final FirebaseAuth mAuth = FirebaseAuth.getInstance();

    // Uri indicates, where the image will be picked from
    private Uri filePath;

    // initialise views
    public SignUpFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                // Handle the back button event
                // initial the LoginFragment and move to to it
                LoginFragment loginFragment = new LoginFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(android.R.id.content, loginFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);

        // The callback can be enabled or disabled here or in handleOnBackPressed()
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        user = FirebaseAuth.getInstance().getCurrentUser(); // get the connected user to app
        /////*     initialize view   */////
        _fullNameText = view.findViewById(R.id.id_fullname_EditText);
        _emailText = view.findViewById(R.id.id_Email_editText);
        _passwordText = view.findViewById(R.id.id_password_editText);
        _signupButton = view.findViewById(R.id.id_signUp_Button);
        _userType = view.findViewById(R.id.userType_radioGroup);
        //TODO: add these fields
//        this.birthday = view.findViewById(R.id.birthday_config_editText);
//        this.phone = view.findViewById(R.id.phone_config_editText);

        this.btnSelect = view.findViewById(R.id.select_image_btn);
        this.imageView = view.findViewById(R.id.profileImageView);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        this.btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectImage();
            }
        });

        // create user
        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupfunction();
            }
        });

        return view;
    }

    // Select Image method
    private void SelectImage() {

        // Defining Implicit Intent to mobile gallery
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(
                        intent,
                        "Select Image from here..."),
                22);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,
                resultCode,
                data);

        // checking request code and result code
        // if request code is PICK_IMAGE_REQUEST and
        // resultCode is RESULT_OK
        // then set image in the image view
        if (requestCode == 22
                && resultCode == RESULT_OK
                && data != null
                && data.getData() != null) {

            // Get the Uri of data
            filePath = data.getData();
            try {

                // Setting image on image view using Bitmap
                Bitmap bitmap = MediaStore
                        .Images
                        .Media
                        .getBitmap(getActivity().getContentResolver(), filePath);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                // Log the exception
                e.printStackTrace();
            }
        }
    }


    // UploadImage method
    private void uploadImage(String email, String fullName) {
        if (filePath != null) {

            // Defining the child of storageReference
            StorageReference ref = storageReference.child("images/" + UUID.randomUUID().toString());

            // adding listeners on upload
            // or failure of image
            ref.putFile(filePath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            imageURL = uri.toString();
                            Log.d("TAG", imageURL);
                            saveData(email, fullName, imageURL);
                            // for the firebaseAuth sign in to account just created
                            Toast.makeText(getContext(), "Created", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            })

                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast
                                    .makeText(getContext(),
                                            "Failed " + e.getMessage(),
                                            Toast.LENGTH_SHORT)
                                    .show();
                            saveData(email, fullName, imageURL);
                        }
                    })
                    .addOnProgressListener(
                            new OnProgressListener<UploadTask.TaskSnapshot>() {

                                // Progress Listener for loading
                                // percentage on the dialog box
                                @Override
                                public void onProgress(
                                        UploadTask.TaskSnapshot taskSnapshot) {
                                    double progress
                                            = (100.0
                                            * taskSnapshot.getBytesTransferred()
                                            / taskSnapshot.getTotalByteCount());
//                                    progressDialog.setMessage(
//                                            "Uploaded "
//                                                    + (int) progress + "%");
                                }
                            });
        } else
            saveData(email, fullName, "");

    }

    private void signupfunction() {
        /////*   Get  Email and Password    */////
        String fullName = _fullNameText.getText().toString();
        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();
        /////*   Check if email and password written and valid   */////
        if (!validate()) {
            return;
        } else {
            // Code for showing progressDialog while uploading
            ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Creating...");
            progressDialog.show();
            signUp(fullName, email, password);
        }
    }

    private void signUp(String fullName, String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("Firebase", "createUserWithEmail:success");
                            user = mAuth.getCurrentUser();
                            uploadImage(email, fullName);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Firebase", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(getContext(), "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void saveData(String email, String fullName, String imageURL) {
        // Check what type of user signed up
        switch (_userType.getCheckedRadioButtonId()) {
            case R.id.trainer_radioButton: // if the user checked the trainer button
                Trainer newTrainer = new Trainer(user.getUid(), email, fullName, "birthday", "phone", imageURL);
                Model.instance.insertTrainer(newTrainer, new ModelSQL.AddListener() {
                    @Override
                    public void onComplete() {
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        startActivity(intent);
//                                            progressDialog.dismiss();
                    }
                });
                break;
            case R.id.participant_radioButton: // if the user checked the participant button
                User newUser = new User(user.getUid(), email, fullName, "birthday", "phone", imageURL);
                Model.instance.insertUser(newUser, new ModelSQL.AddListener() {
                    @Override
                    public void onComplete() {
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        startActivity(intent);
//                                            progressDialog.dismiss();
                    }
                });
                break;
        }
    }

    public boolean validate() {
        boolean valid = true;

        String fullName = _fullNameText.getText().toString();
        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (fullName.isEmpty()) {
            _fullNameText.setError(getString(R.string.enteryourname));
            valid = false;
        } else {
            _fullNameText.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError(getString(R.string.validemail));
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 6 || password.length() > 12) {
            _passwordText.setError(getString(R.string.validpassword));
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }
}