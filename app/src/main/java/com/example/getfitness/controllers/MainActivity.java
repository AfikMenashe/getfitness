package com.example.getfitness.controllers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.example.getfitness.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements MainFragment.OnClickEvent {

    public BottomNavigationView getBottomNavigationView() {
        return bottomNavigationView;
    }

    private BottomNavigationView bottomNavigationView;
    private NavController navController;

    public NavController getNavController() {
        return this.navController;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navController = Navigation.findNavController(this, R.id.fragment);
        //  NavigationUI.setupActionBarWithNavController(this,navController);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigationView);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);
    }


    @Override
    public void onItemClick() {
        this.bottomNavigationView.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {

//        int count = getSupportFragmentManager().getBackStackEntryCount();
//        if (count == 0) {
        super.onBackPressed();
        this.bottomNavigationView.setVisibility(View.VISIBLE);
//        } else {
//            getSupportFragmentManager().popBackStack();
//        }

    }

    public void visibilityOfBottom(boolean isVisible) {
        if (isVisible) // show bottom bar
            this.bottomNavigationView.setVisibility(View.VISIBLE);
        else// hide bottom bar
            this.bottomNavigationView.setVisibility(View.INVISIBLE);
    }

}