package com.example.getfitness.controllers;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.annimon.stream.Stream;
import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.DatePicker;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.builders.DatePickerBuilder;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.applandeo.materialcalendarview.listeners.OnSelectDateListener;
import com.applandeo.materialcalendarview.utils.DateUtils;
import com.example.getfitness.R;
import com.example.getfitness.adapters.CalenderListAdapter;
import com.example.getfitness.adapters.EventListAdapter;
import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.Trainer;
import com.example.getfitness.model.entites.User;
import com.example.getfitness.model.relations.UserWithEvents;
import com.example.getfitness.model.viewmodels.CalenderViewModel;
import com.example.getfitness.model.viewmodels.EventListViewModel;
import com.example.getfitness.model.viewmodels.ProfileViewModel;
import com.example.getfitness.model.Model;
import com.example.getfitness.model.entites.ImageUrl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;

public class CalenderFragment extends Fragment {

    CalenderViewModel calenderViewModel;

    public CalenderFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calender, container, false);

        // <------------- Calendar View ------------->

        CalendarView calendarView = (CalendarView) view.findViewById(R.id.calendarView);

        // <------------- Recycler View ------------->

        RecyclerView calenderView = view.findViewById(R.id.calenderRecyclerView);
        calenderView.setHasFixedSize(true);

        LinearLayoutManager layoutMananger = new LinearLayoutManager(view.getContext());
        calenderView.setLayoutManager(layoutMananger);
        CalenderListAdapter calenderListAdapter = new CalenderListAdapter();
        calenderView.setAdapter(calenderListAdapter);

        // <------------- View Model ------------->

        calenderViewModel = new ViewModelProvider(this).get(CalenderViewModel.class); // get the viewModel for holding the data for this fragment
        //// In case the correct user is Trainer -> search for the events the user sign up to
        calenderViewModel.getUserEvents().observe(getViewLifecycleOwner(), new Observer<UserWithEvents>() {
            @Override
            public void onChanged(UserWithEvents userWithEvents) {
                if (userWithEvents != null) {
                    if (userWithEvents.events.size() > 0) { // verify the list is more than 1
                        calenderListAdapter.setEvents(filterEventsByDate(userWithEvents.events, new Date()));
                    }
                } else

                    // In case the correct user is Trainer -> search for the events he created.
                    calenderViewModel.getTrainerEvents().observe(getViewLifecycleOwner(), new Observer<List<Event>>() {
                        @Override
                        public void onChanged(List<Event> events) {
                            if (events.size() > 0) { // verify the list is more than 1
                                calenderListAdapter.setEvents(filterEventsByDate(events, new Date()));
                            }
                        }
                    });
            }
        });

        // if the user/trainer click on the event -> forward to Event page
        calenderListAdapter.setOnClickListener(new CalenderListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Event clickedEvent = calenderListAdapter.calendersList.get(position);
                Navigation.findNavController(view).navigate(CalenderFragmentDirections.actionCalenderFragmentToEventFragment(clickedEvent.getEventId()));
            }
        });

        // listener when click on a day
        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
//                eventDay.getCalendar().getTime().toString()
                List<Event> trainersEventList = calenderViewModel.getTrainerEvents().getValue();
                UserWithEvents usersEventList = calenderViewModel.getUserEvents().getValue();
                if (usersEventList != null)
                    calenderListAdapter.setEvents(filterEventsByDate(usersEventList.events, new Date(eventDay.getCalendar().getTime().getTime())));
                else if (trainersEventList != null)
                    calenderListAdapter.setEvents(filterEventsByDate(trainersEventList, new Date(eventDay.getCalendar().getTime().getTime())));

            }
        });
        return view;
    }

    private List<Event> filterEventsByDate(List<Event> events, Date date) {
        List<Event> filtredList = new ArrayList<>();
        if (events != null) {
            for (int i = 0; i < events.size(); i++) {
                // insert to List only events with the same date
                if (events.get(i).getDate().getDate() == date.getDate() && events.get(i).getDate().getMonth() == date.getMonth() && events.get(i).getDate().getYear() == date.getYear())
                    filtredList.add(events.get(i));

            }
            // sort the filtered list by time
            Collections.sort(filtredList, new SortbyDate());
        }
        return filtredList;
    }

    class SortbyDate implements Comparator<Event> {
        // Used for sorting in ascending order of
        public int compare(Event a, Event b) {
            if (a.getDate().after(b.getDate()))
                return 1;
            else if (a.getDate().before(b.getDate()))
                return -1;
            else return 0;
        }
    }
}