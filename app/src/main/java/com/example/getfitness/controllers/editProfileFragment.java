package com.example.getfitness.controllers;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.example.getfitness.R;
import com.example.getfitness.model.Model;
import com.example.getfitness.model.ModelSQL;
import com.example.getfitness.model.entites.Trainer;
import com.example.getfitness.model.entites.User;
import com.example.getfitness.model.viewmodels.ProfileViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

public class editProfileFragment extends Fragment {

    private EditText nameTextView;
    private EditText emailTextView;
    private EditText phoneTextView;
    private TextView birthdayTextView;
    ProfileViewModel profileViewModel;
    private CircleImageView profile_imageView;

    private Calendar dateSelected;

    public editProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profileViewModel = new ViewModelProvider(this).get(ProfileViewModel.class); // get the viewModel for holding the data for this fragment

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        dateSelected = Calendar.getInstance();

        // <----------------- find the the elements in xml by ID ----------------->
        nameTextView = view.findViewById(R.id.edit_user_name_txt);
        emailTextView = view.findViewById(R.id.edit_user_email_txt);
        phoneTextView = view.findViewById(R.id.edit_user_phone_txt);
        birthdayTextView = view.findViewById(R.id.edit_user_birthday_txt);
        profile_imageView = view.findViewById(R.id.edit_profile_imageView);

        birthdayTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDateTimeField();
            }
        });

        // <----------------- assign the user data to UI ----------------->

        nameTextView.setText(editProfileFragmentArgs.fromBundle(getArguments()).getUserName());
        emailTextView.setText(editProfileFragmentArgs.fromBundle(getArguments()).getUserEmail());
        phoneTextView.setText(editProfileFragmentArgs.fromBundle(getArguments()).getUserPhone());
        birthdayTextView.setText(editProfileFragmentArgs.fromBundle(getArguments()).getUserBirthday());
        Picasso.get().load(editProfileFragmentArgs.fromBundle(getArguments()).getUserImgUrl()).fit().into(profile_imageView);

        FloatingActionButton fab = view.findViewById(R.id.save_profile_floating_btn);
        // <-- assign the the listener to floating btn when clicked navigate to edit Profile  -->
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userId, name, email, phone, birthday, imageurl, type;
                userId = editProfileFragmentArgs.fromBundle(getArguments()).getUserId();
                name = nameTextView.getText().toString();
                email = emailTextView.getText().toString();
                phone = phoneTextView.getText().toString();
                birthday = birthdayTextView.getText().toString();
                imageurl = editProfileFragmentArgs.fromBundle(getArguments()).getUserImgUrl();
                type = editProfileFragmentArgs.fromBundle(getArguments()).getUserType();

                if (type.equals("user")) {
                    User editedUser = new User();
                    editedUser.setAll(userId, email, name, birthday, phone, imageurl);
                    Model.instance.insertUser(editedUser, new ModelSQL.AddListener() {
                        @Override
                        public void onComplete() {
                            Navigation.findNavController(view).navigate(R.id.action_editProfileFragment_to_profileFragment);
                        }
                    });
                } else {
                    Trainer editedUser = new Trainer();
                    editedUser.setAll(userId, email, name, birthday, phone, imageurl);
                    Model.instance.insertTrainer(editedUser, new ModelSQL.AddListener() {
                        @Override
                        public void onComplete() {
                            Navigation.findNavController(view).navigate(R.id.action_editProfileFragment_to_profileFragment);
                        }
                    });
                }
            }
        });

        return view;
    }

    private void setDateSelected(Calendar dateSelected) {
        Toast.makeText(getActivity(), dateSelected.getTime().toString(), Toast.LENGTH_LONG).show();
        birthdayTextView.setText(dateSelected.getTime().toString());
    }

    private void setDateTimeField() {
        Calendar newCalendar = dateSelected;
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dateSelected.set(year, monthOfYear, dayOfMonth, 0, 0);
                setDateSelected(dateSelected);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
}