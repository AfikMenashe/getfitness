package com.example.getfitness.controllers;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.getfitness.adapters.EventImagesListAdapter;
import com.example.getfitness.adapters.ImageListAdapter;
import com.example.getfitness.R;
import com.example.getfitness.model.Model;
import com.example.getfitness.model.ModelSQL;
import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.ImageUrl;
import com.example.getfitness.model.entites.Trainer;
import com.example.getfitness.model.entites.Type;
import com.example.getfitness.model.entites.User;
import com.example.getfitness.model.relations.EventUser;
import com.example.getfitness.model.relations.EventWithImageUrls;
import com.example.getfitness.model.relations.EventWithTypes;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventFragment extends Fragment {

    private RecyclerView imageList;
    private EventImagesListAdapter eventImagesListAdapter;
    private TextView dateTextView;
    private TextView trainerNameTextView;
    private TextView locationTextView;
    private TextView tagsTextView;
    private TextView descriptionTextView;
    private TextView titleTextView;
    private TextView priceTextView;
    private Button signUP_btn;
    private LiveData<Event> eventLiveData;
    private LiveData<EventUser> eventUserLiveData;
    private EventUser eventUser;
    private int signedUsers;
//    private ArrayList<ImageUrl> listForRecycle;

    final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    String eventId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.eventId = EventFragmentArgs.fromBundle(getArguments()).getEventId(); // get the ID from navigation
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event, container, false);

        Model.instance.getEventWithImageUrls(this.eventId).observe(getViewLifecycleOwner(), new Observer<List<EventWithImageUrls>>() {
            @Override
            public void onChanged(List<EventWithImageUrls> eventWithImageUrls) {
                eventImagesListAdapter.setList(eventWithImageUrls.get(0).imageUrls);
            }
        });
//        imageList.setHasFixedSize(true);

        //------------------------- Set view controls references -------------------------//
        dateTextView = view.findViewById(R.id.date_time_id);
        trainerNameTextView = view.findViewById(R.id.trainer_name_id);
        locationTextView = view.findViewById(R.id.location_id);
        tagsTextView = view.findViewById(R.id.train_tags_id);
        descriptionTextView = view.findViewById(R.id.description_id);
        titleTextView = view.findViewById(R.id.event_title_id);
        signUP_btn = view.findViewById(R.id.sign_up_btn);
        imageList = view.findViewById(R.id.image_list);
        priceTextView = view.findViewById(R.id.price_id);

        //------------------------- Set listeners -------------------------//
        eventUserLiveData = Model.instance.getEventUser(eventId, currentUser.getUid());
        //observer if the user is joined the event or not

        //------------------------- Bundle RecycleList and adapter -------------------------//

        eventImagesListAdapter = new EventImagesListAdapter();
//        eventImagesListAdapter.setList(listForRecycle);
        imageList.setAdapter(eventImagesListAdapter);
        imageList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,false));

        //------------------------- Get the EventTypes -------------------------//

        getEventTypes(eventId);

        //------------------------- Get the Event -------------------------//

        eventLiveData = Model.instance.getEvent(eventId);
        eventLiveData.observe(getViewLifecycleOwner(), new Observer<Event>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onChanged(Event event) {
                FloatingActionButton fab = view.findViewById(R.id.edit_floating_btn);
                // <-- assign the the listener to floating btn when clicked navigate to edit Event  -->
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Navigation.findNavController(view).navigate(EventFragmentDirections.actionEventFragmentToEditEventFragment(event.getEventId()));
                    }
                });

                dateTextView.setText(event.getDate().toString());
                locationTextView.setText(event.getCity() + event.getStreet());
                descriptionTextView.setText(event.getDescription());
                titleTextView.setText(event.getTitle());
                priceTextView.setText(String.valueOf(event.getPrice()));
                getTrainerName(event.getTrainerCreatorId()); // Get the Trainer name
                if (event.getTrainerCreatorId().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                    fab.setVisibility(View.VISIBLE);
                    signUP_btn.setBackgroundColor(Color.rgb(255, 0, 0));
                    signUP_btn.setText("Delete");
                    signUP_btn.setOnClickListener(v -> {
                        Model.instance.deleteEvent(event, new ModelSQL.AddListener() {
                            @Override
                            public void onComplete() {
                                Navigation.findNavController(view).navigate(R.id.action_eventFragment_to_mainFragment);
                            }
                        });
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).getBottomNavigationView().setVisibility(View.VISIBLE);
                    });
                } else
                    setJoinListener(event.getSignedUsers());
            }
        });

        return view;
    }

    //----------------------------Custom methods----------------------------//
    private void getEventTypes(String eventId) {
        // get the event types and add to screen
        Model.instance.getEventWithTypes(eventId).observe(getViewLifecycleOwner(), new Observer<EventWithTypes>() {
            @Override
            public void onChanged(EventWithTypes eventWithTypes) {
                if (!eventWithTypes.types.isEmpty()) {
                    StringBuffer eventTypes = new StringBuffer();
                    int i = 0;
                    // create string -> Kickbox,Running....
                    for (i = 0; i < eventWithTypes.types.size() - 1; i++) {
                        eventTypes.append(eventWithTypes.types.get(i).getName());
                        eventTypes.append(',');
                    }
                    eventTypes.append(eventWithTypes.types.get(i).getName());

                    tagsTextView.setText(eventTypes);
                }
            }
        });
    }

    private void getTrainerName(String trainerId) {
        // get the Trainer name and assign in screen
        Model.instance.getTrainer(trainerId).observe(getViewLifecycleOwner(), new Observer<Trainer>() {
            @Override
            public void onChanged(Trainer trainer) {
//                if(trainer.getName() != null)
                trainerNameTextView.setText(trainer.getName());
            }
        });
    }


    private void setJoinListener(int signedUsers) {
        this.signedUsers = signedUsers;
        // check if the user is join or not join to the event -> check in UserEvent
        eventUserLiveData.observe(getViewLifecycleOwner(), new Observer<EventUser>() {
            @Override
            public void onChanged(EventUser returnEventUser) {
                if (returnEventUser == null) {
                    signUP_btn.setText("Join");
                } else {
                    eventUser = returnEventUser;
                    if (returnEventUser.getDeleted()) {
                        signUP_btn.setText("join");
                    } else {
                        signUP_btn.setText("unJoin");
                    }
                }
            }
        });
        //remove the observer from it

        signUP_btn.setOnClickListener(v -> {
            if (this.eventUser != null)
                if (!this.eventUser.getDeleted())
                    disjoinEvent();
                else
                    joinEvent();
            else
                joinEvent();
//            if ((MainActivity) getActivity() != null)
//                ((MainActivity) getActivity()).getBottomNavigationView().setVisibility(View.VISIBLE);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (this.eventUser != null) {
            if (!this.eventUser.getDeleted()) {
                Model.instance.addCountSignUserToEvent(this.eventUser.eventId, this.signedUsers + 1);
                Model.instance.addUserToEvent(this.eventUser, null);
            } else {
                Model.instance.deleteUserFromEvent(this.eventUser);
                Model.instance.removeCountSignUserToEvent(this.eventUser.eventId, this.signedUsers - 1);
            }
        }
        // update the event
//        Model.instance.insertEvent(this.eventLiveData.getValue(), null);
    }

    // <----------------------------- Custom functions ----------------------------->


    protected void joinEvent() {
        signUP_btn.setText("unJoin");
        //Get the Event from the model
        String eventId = eventLiveData.getValue().getEventId();
        //Create event user obj for inert to DB -> will happen at the exit of the fragment
        if (this.eventUser == null)
            this.eventUser = new EventUser(eventId, currentUser.getUid(), false);
        else
            this.eventUser.setDeleted(false);
        //Increase event capacity
        this.eventLiveData.getValue().increaseSignedUsers();
    }

    protected void disjoinEvent() {
        signUP_btn.setText("join");
        //Get the Event from the model
        String eventId = eventLiveData.getValue().getEventId();
        //Create event user obj for inert to DB -> will happen at the exit of the fragment
        if (this.eventUser == null)
            this.eventUser = new EventUser(eventId, currentUser.getUid(), true);
        else
            this.eventUser.setDeleted(true);
        //Decrease event capacity
        this.eventLiveData.getValue().decreaseSignedUsers();
    }

    protected void writeReview(String trainer_id, String review_content) {
        //Get the Trainer from the model
        //Generate new review and set the content
        //Add the review to trainer list
        //Update DB
    }
}